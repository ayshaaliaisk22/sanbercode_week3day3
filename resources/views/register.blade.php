<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
        <label for="firstname">First name:</label><br><br>
        <input type="text" name="firstname">

        <br><br>

        <label for="lastname">Last name:</label><br><br>
        <input type="text" name="lastname">

        <br><br>
        
        <label for="gender">Gender:</label><br><br>
        <input type="radio" name="gender">Male <br>
        <input type="radio" name="gender">Female <br>
        <input type="radio" name="gender">Other

        <br><br>

        <label for="">Nationality</label><br><br>
        <select name="nationality" id="">
            <option value="indonesian">Indonesian</option>
            <option value="american">American</option>
            <option value="arabian">Arabian</option>
            <option value="other">Other</option>
        </select>
        
        <br><br>

        <label for="language">Language Spoken:</label><br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other

        <br><br>

        <label for="bio">Bio:</label><br><br>
        <textarea name="bio" id="" cols="30" rows="10"></textarea>

        <br><br>

        <input type="submit" value="Sign Up">
    </form>
    
</body>
</html>