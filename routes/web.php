<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Facade\FlareClient\View;
use Illuminate\Routing\Route;

// Route::get('/', 'HomeController@home');

// Route::get('/register', 'AuthController@register');

// Route::post('/welcome', 'AuthController@welcome');

Route::get('/table', function(){
    return view('table');
});

Route::get('/data-tables', function(){
    return view('data_tables');
});

Route::get('/master', function(){
    return view('adminlte.master');
});

// Route::get('/items', function(){
//     return view('items.index');
// });
Route::get('/cast', 'CastController@index');
Route::post('/cast', 'CastController@store');
Route::get('/cast/create', 'CastController@create');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete'/cast/{cast_id}', 'CastController@destroy');