<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function welcome(Request $request){
        $firstname = $request["firstname"];
        $lastname = $request["lastname"];

        echo "<h1>SELAMAT DATANG, $firstname $lastname! </h1>";
        echo "<h2>Terima kasih telah bergabung di SanberBook. Social Media Kita Bersama!</h2>";
        // return view('welcome');
    }
}
